<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Imglist;

/**
 * ImgContainer
 *
 * @ORM\Table(name="img_container")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ImgContainerRepository")
 */
class ImgContainer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
     /**
     * @var \Doctrine\Common\Collections\Collection
     */
    protected $images;

    public function __toString()
    {
        return $this->name ? $this->name : '';
    }

     /**
     * Constructor
     */
    public function __construct()
    {
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

   /**
     * Add images
     *
     * @param \AppBundle\Entity\Imglist $image
     * @return ImgContainer
     */
    public function addImage(Imglist $image)
    {
        $this->images[] = $image;
        $image->setBike($this);

        return $this;
    }

    /**
     * Remove images
     *
     * @param \AppBundle\Entity\Imglist $image
     */
    public function removeImage(Imglist $image)
    {
        $this->images->removeElement($image);
        $image->setBike(null);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }
}

