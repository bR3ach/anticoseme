<?php

// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @var ArrayCollection
     * 
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Post", mappedBy="user")
     */
    protected $posts;
    /**
     * @ORM\Column(type="text")          
     */
    protected $propic;

    public function __construct()
    {
        parent::__construct();
        $this->posts = new ArrayCollection();        
    }
    /**
     * Add posts
     *
     * @param \AppBundle\Entity\Post $posts
     * @return User
     */
    public function addPosts(\AppBundle\Entity\Post $posts)
    {
        $this->posts[] = $posts;
 
        return $this;
    }
 
    /**
     * Remove posts
     *
     * @param \AppBundle\Entity\Posts $posts
     */
    public function removePost(\AppBundle\Entity\Post $posts)
    {
        $this->posts->removeElement($posts);
    }
 
    /**
     * Get posts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPosts()
    {
        return $this->posts;
    }
    public function getPropic()
    {
        return $this->propic;
    }
    public function setPropic($propic)
    {
        $this->propic = $propic;
    }


}