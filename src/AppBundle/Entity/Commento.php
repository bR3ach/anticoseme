<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Commento
 *
 * @ORM\Table(name="commento")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CommentoRepository")
 */
class Commento
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="autore", type="string", length=75)
     */
    private $autore;

    /**
     * @var string
     *
     * @ORM\Column(name="propic", type="text")
     */
    private $propic;

    /**
     * @var string
     *
     * @ORM\Column(name="contenuto", type="text")
     */
    private $contenuto;

    /**
     * @var int
     *
     * @ORM\Column(name="idpost", type="integer")
     */
    private $idpost;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data", type="datetime")
     */
    private $data;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set autore
     *
     * @param string $autore
     *
     * @return Commento
     */
    public function setAutore($autore)
    {
        $this->autore = $autore;

        return $this;
    }

    /**
     * Get autore
     *
     * @return string
     */
    public function getAutore()
    {
        return $this->autore;
    }

    /**
     * Set propic
     *
     * @param string $propic
     *
     * @return Commento
     */
    public function setPropic($propic)
    {
        $this->propic = $propic;

        return $this;
    }

    /**
     * Get propic
     *
     * @return string
     */
    public function getPropic()
    {
        return $this->propic;
    }

    /**
     * Set contenuto
     *
     * @param string $contenuto
     *
     * @return Commento
     */
    public function setContenuto($contenuto)
    {
        $this->contenuto = $contenuto;

        return $this;
    }

    /**
     * Get contenuto
     *
     * @return string
     */
    public function getContenuto()
    {
        return $this->contenuto;
    }

    /**
     * Set idpost
     *
     * @param integer $idpost
     *
     * @return Commento
     */
    public function setIdpost($idpost)
    {
        $this->idpost = $idpost;

        return $this;
    }

    /**
     * Get idpost
     *
     * @return int
     */
    public function getIdpost()
    {
        return $this->idpost;
    }

    /**
     * Set data
     *
     * @param \DateTime $data
     *
     * @return Commento
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return \DateTime
     */
    public function getData()
    {
        return $this->data;
    }
}

