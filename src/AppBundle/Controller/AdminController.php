<?php
namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use JavierEguiluz\Bundle\EasyAdminBundle\Controller\AdminController as EasyAdminController;

class AdminController extends EasyAdminController
{
    
    public function indexAction(Request $request)
    {

        return parent::indexAction($request);
    }
    
    public function createNewUserEntity()
{
    return $this->get('fos_user.user_manager')->createUser();
}
}
?>