<?php 


namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


 class InCucinaController extends Controller
 {
 	
 	
 	function indexAction()
 	{
 		return $this->render('in_cucina/index.html.twig');
 	}
 	/**
     * @Route("/in_cucina/primi" , name = "primi")
     */ 
 	public function showPrimiAction() {
 		$piatti = $this->getDoctrine()
        ->getRepository('AppBundle:Post')
        ->findByCategory('primi');
 		return $this->render('in_cucina/primi.html.twig', array('piatti' => $piatti));
 	}
 	/**
     * @Route("/in_cucina/secondi" , name = "secondi")
     */ 
 	public function showSecondiAction() {

 		return $this->render('in_cucina/secondi.html.twig');
 	}
 	/**
     * @Route("/in_cucina/dolci" , name = "dolci")
     */ 
 	public function showDolciAction() {
 		return $this->render('in_cucina/dolci.html.twig');
 	}
 } 
 
?>