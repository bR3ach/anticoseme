<?php 


namespace AppBundle\Controller;

use \DateTime;
use AppBundle\Entity\Post;
use AppBundle\Entity\Commento;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\Request;
use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;



 class BlogController extends Controller
 {
 	/**
     * @Route("/blog" , name = "blog")
     */ 	
 	function indexAction(Request $request)
 	{ 	
 		$blog_posts = $this->getDoctrine()
        ->getRepository('AppBundle:Post')
        ->findByCategory('blog');
 		return $this->render('blog/index.html.twig', array('posts' => $blog_posts ));
 	}

 	/**
     * @Route("/blog/{slug}" , name = "blog_entry")
     */
    public function showAction($id , Post $post, Request $request)
    {   
        $session = $request->getSession();      
        $fb = new Facebook([
                  'app_id' => '224740781222603', 
                  'app_secret' => '753537793a210302a7627dfa17c76b83',
                  'default_graph_version' => 'v2.5',
                   ]);

        $helper = $fb->getRedirectLoginHelper();
        $permissions = ['email', ' publish_action']; 
        $loginUrl = $helper->getLoginUrl('http://lochost.it/symprog/anticoseme/web/app_dev.php/blog/callback/'.$post->getSlug().'/'.$id , $permissions);        
       
        $comments = $this->getDoctrine()
                    ->getRepository("AppBundle:Commento")->findBy(array('idpost' => $id),array('data' => 'DESC')) ;
        $autore = $this->getDoctrine()
                       ->getRepository('AppBundle:User')
                       ->find($post->getUser());          

        $commento = new Commento();
        $form = $this->createFormBuilder($commento)
            ->add('contenuto', TextType::class)            
            ->add('save', SubmitType::class, array('label' => 'Pubblica'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $commento = $form->getData(); 
            $commento->setData(new DateTime('now'));
            $commento->setIdpost($id); 

            if( $this->getUser() ){
                  $name = $this->getUser();
                  $commento->setAutore($this->getUser());
                  $user = $this->getUser();  
                  $commento->setPropic($user->getPropic());
            }
            else { 
                  $commento->setAutore($session->get('fbuser'));
                  $commento->setPropic($session->get('propic'));}            
                       
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($commento);
            $entityManager->flush();

        return $this->redirect($this->generateUrl('blog_entry', array('id'=>$id, 'slug' => $slug)));
    }           

        return $this->render('blog/post.detail.html.twig', array( 'post' => $post, 'autore' => $autore, 'commenti' => $comments ,'loginUrl' => $loginUrl, 'form' => $form->createView()));        
    }
    /**
     * @Route("/blog/callback/{slug}/{id}", name = "callback")
     */
    public function callbackAction( $slug , $id , Request $request) 
    {
         $session = $request->getSession();

          $fb = new Facebook([
                'app_id' => '224740781222603',
                'app_secret' => '753537793a210302a7627dfa17c76b83',
                'default_graph_version' => 'v2.5',
                ]);
          $helper = $fb->getRedirectLoginHelper();
          $permissions = ['email']; 
  
         try {
         if ($session->has('facebook_access_token')) {
            $accessToken = $session->get('facebook_access_token');
            } else {
            $accessToken = $helper->getAccessToken();
            }
            } catch(Facebook\Exceptions\FacebookResponseException $e) {
                    $request->getSession()
                    ->getFlashBag()
                    ->add('error', $e->getMessage());  
   
            } catch(Facebook\Exceptions\FacebookSDKException $e) {
                    $request->getSession()
                    ->getFlashBag()
                    ->add('error', $e->getMessage());   
            }
         if (isset($accessToken)) {
             if ($session->has('facebook_access_token')) {
               $fb->setDefaultAccessToken($session->get('facebook_access_token'));
               } 
            else {
                // getting short-lived access token
                $session->set('facebook_access_token', $accessToken);
                // OAuth 2.0 client handler
                $oAuth2Client = $fb->getOAuth2Client();
                // Exchanges a short-lived access token for a long-lived one
                $longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($session->get('facebook_access_token'));
                $session->set('facebook_access_token', $longLivedAccessToken);
                // setting default access token to be used in script
                $fb->setDefaultAccessToken($session->get('facebook_access_token'));
                }  
               // getting basic info about user
         try {
             $profile_request = $fb->get('/me?fields=name,picture');
             $profile = $profile_request->getGraphNode();
             $fbname = $profile->getField('name');
             $fbpic = $profile->getField('picture')->getField('url');
             $session->set('fbuser', $fbname);
             $session->set('propic', $fbpic);
             }
         catch(Facebook\Exceptions\FacebookResponseException $e) {
               $request->getSession()
                       ->getFlashBag()
                       ->add('error', $e->getMessage()); 
               $session->invalidate();    
       } catch(Facebook\Exceptions\FacebookSDKException $e) {
        // When validation fails or other local issues
               $request->getSession()
               ->getFlashBag()
               ->add('error', $e->getMessage());     
             } 
  
  return $this->redirect($this->generateUrl('blog_entry', array('id'=>$id, 'slug' => $slug)));
}

    }
   
 } 
 
?>